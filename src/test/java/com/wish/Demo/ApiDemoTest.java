package com.wish.Demo;

import java.util.List;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import com.empl.mgr.activiti.utils.ApiDemo;

public class ApiDemoTest {

	ApiDemo activitiApiDemo = new ApiDemo();

	Deployment gDeployment;

	ProcessInstance gProcessInstance;

	@Test
	public void initTest() {

		/*// 部署流程定义
		gDeployment = activitiApiDemo.deploymentProcessDefinition_classpath("diagrams/apiDemo/Demo.bpmn",
				"diagrams/apiDemo/Demo.png");

		// 启动部署流程方式1（建议使用此方式）
		gProcessInstance = activitiApiDemo.startProcessInstanceByKey("DemoProcess");

		// 启动部署流程方式2
		// gProcessInstance =
		// activitiApiDemo.startProcessInstanceById(gDeployment.getId());

		// 查找该部署流程中所有任务（并将张三所有的全部完成）
		
		List<Task> allTaskList = activitiApiDemo.findAllTask();
		for (Task task : allTaskList) {
			activitiApiDemo.completeMyProcessTask(task.getId());
		}
		 

		// 查询当前所有任务
		System.out.println("\n\n#############--------查询当前所有任务：");
		activitiApiDemo.findAllTask();

		*//**
		 * 查询张三所有任务，并将张三的任务完成(默认使用张三自己完成任务)
		 * 张三任务完成时，因为添加了activiti:executionListener,将会自动调用Person里面的excute方法
		 * 其中ExecutionListener监听器可以配置属性为：start/end/take
		 * 同时张三任务完成，根据Demo.bpmn流程图，自动流转到李四部门经理批准任务
		 *//*
		String assignee = "张三";
		System.out.println("\n\n#############--------查询张三当前所有任务，并执行完所有张三任务：");
		List<Task> myTaskList = activitiApiDemo.findMyTaskByAssignee(assignee);
		for (Task task : myTaskList) {
			// 此处可以将任务处理人设置为ttx
			// activitiApiDemo.setTaskAssignee(task.getId(), "ttx");
			activitiApiDemo.completeMyProcessTask(task.getId());
		}

		// 当前任务认领
		// taskService.claim(task.getId(), "fozzie");

		System.out.println("\n\n#############--------查询当前所有任务（由于上述张三将所有任务完成了，自动流转到李四部门经理审批任务）：");
		activitiApiDemo.findAllTask();*/
		activitiApiDemo.delProcessProcessInstance("27501", "no why");;

		/**
		 * 查询李四所有任务，并将李四的任务完成(默认使用李四自己完成任务)
		 * 李四任务完成时，因为添加了activiti:executionListener,将会自动调用Person里面的excute方法
		 * 其中ExecutionListener监听器可以配置属性为：start/end/take
		 * 同时李四任务完成，根据Demo.bpmn流程图，自动流转到下一个批准任务
		 *//*
		String assigneeLisi = "李四";
		System.out.println("\n\n#############--------查询李四当前所有任务，并执行完所有李四任务：");
		List<Task> lisiTaskList = activitiApiDemo.findMyTaskByAssignee(assignee);
		for (Task task : lisiTaskList) {
			// 此处可以将任务处理人设置为ttx
			// activitiApiDemo.setTaskAssignee(task.getId(), "ttx");
			activitiApiDemo.completeMyProcessTask(task.getId());
		}
		
		System.out.println("\n\n#############--------查询当前所有任务（由于上述李四将所有任务完成了，自动流转到下一个审批任务）：");
		activitiApiDemo.findAllTask();*/
		
		/*gProcessInstance = activitiApiDemo.startProcessInstanceByKey("DemoProcess");
		activitiApiDemo.queryHistoricProcessInstance("5");*/
		
		activitiApiDemo.findAllTask();
		
		/*activitiApiDemo.setTaskAssignee("12511", assigneeWangwu);
		activitiApiDemo.setTaskAssignee("10011", assigneeWangwu);
		
		System.out.println("\n\n#############--------查询王五当前所有任务");
		List<Task> wangwuTaskList = activitiApiDemo.findMyTaskByAssignee(assigneeWangwu);

		
		//完成王五的 12511 任务
		for (Task task : wangwuTaskList) {
			System.out.println(task.getId());
			if(task.getId().equals("12511")){
				activitiApiDemo.completeMyProcessTask(task.getId());
			}
			
		}
		activitiApiDemo.completeMyProcessTask("32504");
		activitiApiDemo.findMyTaskByAssignee("王五");
		activitiApiDemo.findAllTask();*/
		
	}

}
