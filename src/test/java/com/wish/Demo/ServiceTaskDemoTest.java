package com.wish.Demo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Before;
import org.junit.Test;

import com.empl.mgr.activiti.utils.ApiDemo;

public class ServiceTaskDemoTest {
	
	ApiDemo activitiApiDemo = new ApiDemo();
	
	Deployment gDeployment;
	
	ProcessInstance gProcessInstance;
	ProcessEngine processEngine;
	TaskService taskService;
	RepositoryService repositoryService;
	RuntimeService runtimeService;
	FormService formService;
	HistoryService historyService;
	IdentityService identityService;
	ManagementService managementService;

	/**
	 * 初始化方法
	 */
	@Before
	public void initTest(){
		processEngine = activitiApiDemo.getProcessEngine();
		
		repositoryService = processEngine.getRepositoryService();  
        runtimeService = processEngine.getRuntimeService();  
        taskService = processEngine.getTaskService();  
        formService = processEngine.getFormService();  
        historyService = processEngine.getHistoryService();  
        identityService = processEngine.getIdentityService();  
        managementService = processEngine.getManagementService();  
		
		// 部署流程定义
		// 其中png文件自动保存：window ——> preferences——>activiti——>save Action
		// 选中create process definition image when saving the diagram
		gDeployment = activitiApiDemo.deploymentProcessDefinition_classpath("diagrams/serviceTaskDemo/serviceTaskDemo.bpmn", "diagrams/serviceTaskDemo/serviceTaskDemo.png");
	}

	/**
	 * TODO 测试排他网关（请注意：排他网关会一次判断条件是否满足，满足则选择一个路径执行，
	 *                   假若需要用到条件：则需要对排他网关所有路径和为全集条件）
	 * @author ttx
	 * @since 2016年2月24日 上午10:56:18
	 */
	@Test
	public void startProcess(){
		String name = "张三";
		int days = 2;
		Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", name);
        variables.put("days", days);
		
		// 启动部署流程方式1（建议使用此方式）
		gProcessInstance = activitiApiDemo.startProcessInstanceByKey("leaveProcess", variables);
		
		
		
		
		
	
	}
}
