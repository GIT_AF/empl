package com.empl.mgr.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.empl.mgr.constant.PageConstant;
import com.empl.mgr.dao.support.AbstractDao;
import com.empl.mgr.model.TeLeave;

@Repository
public class LeaveDao extends AbstractDao<TeLeave> {

	@Override
	public Class<TeLeave> getEntityClass() {
		// TODO Auto-generated method stub
		return TeLeave.class;
	}
	/**
	 * 获取全部请假记录列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TeLeave> findLeaveList(int page) {
		StringBuffer query = new StringBuffer();
		query.append("from TeLeave where 1=1");
		
		return findSession().createQuery(query.toString()).setFirstResult((page - 1) * PageConstant.PAGE_LIST)
				.setMaxResults(PageConstant.PAGE_LIST).list();
	}
	/**
	 * 获取页码
	 * 
	 * @return
	 */
	public int findLeavePage() {
		
		StringBuffer query = new StringBuffer();
		query.append("select count(*) from TeLeave where 1=1");
		return Integer.parseInt(findSession().createQuery(query.toString()).uniqueResult().toString());
		
		
	}
	
	
}
