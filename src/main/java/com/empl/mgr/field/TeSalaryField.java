package com.empl.mgr.field;

public class TeSalaryField {

	//property constants
	public static final String SALARY_ID = "salaryId";
	public static final String EM_ID = "employeesId";
	public static final String BASE_SALARY = "baseSalary";
	public static final String LATE_SALARY = "lateSalary";
	public static final String ABSENCE_SALARY = "absenceSalary";
	public static final String WUXIANYIJIN = "wuxianyijin";
	public static final String OVERTIME_SALARY = "overtimeSalary";
	public static final String BONUS_SALARY = "bonusSalary";
	public static final String SALARY_TIME = "salaryTime";
	

}