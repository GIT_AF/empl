package com.empl.mgr.activiti.utils;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;

public class ServiceTaskDemo {
	

	ApiDemo activitiApiDemo = new ApiDemo();

	Deployment gDeployment;

	ProcessInstance gProcessInstance;
	
	
	@Test
	public void initTest() {
		// 部署流程定义
		gDeployment = activitiApiDemo.deploymentProcessDefinition_classpath("diagrams/serviceTaskDemo/serviceTaskDemo.bpmn",
				"diagrams/serviceTaskDemo/serviceTaskDemo.png");

		// 启动部署流程方式（建议使用此方式）
		gProcessInstance = activitiApiDemo.startProcessInstanceByKey("ServiceTaskDemoProcess");
		
		// 查询当前所有任务
		System.out.println("\n\n#############--------查询当前所有任务：");
		activitiApiDemo.findAllTask();
		
	}
	

}
