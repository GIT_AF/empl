package com.empl.mgr.dto;

import java.io.Serializable;

/**
 * 薪水基本信息类
 * @author zou
 * 将数据库里的员工信息封闭到此类中, 可以屏蔽关键信息, 也能避免暴露数据库字段
 */
public class SalaryInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id; // ID号	
	private String employeesName; // 员工姓名
	private long emId; // 员工id
	private boolean sex; // 性别
	private String deparemtn; // 部门ID
	
	private double baseSalary; //基本工资
	private double lateSalary; //迟到扣薪
	private double absenceSalary; //缺勤扣薪
	private double wuxianyijin; //五险一金
	private double overtimeSalary; //加班工资
	private double bonusSalary; //奖金
	private String salaryTime; //工资月份

	

	public SalaryInfoDto() {
		// TODO Auto-generated constructor stub
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getEmployeesName() {
		return employeesName;
	}



	public void setEmployeesName(String employeesName) {
		this.employeesName = employeesName;
	}



	public long getEmId() {
		return emId;
	}



	public void setEmId(long emId) {
		this.emId = emId;
	}



	public boolean isSex() {
		return sex;
	}



	public void setSex(boolean sex) {
		this.sex = sex;
	}



	public String getDeparemtn() {
		return deparemtn;
	}



	public void setDeparemtn(String deparemtn) {
		this.deparemtn = deparemtn;
	}



	public double getBaseSalary() {
		return baseSalary;
	}



	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}



	public double getLateSalary() {
		return lateSalary;
	}



	public void setLateSalary(double lateSalary) {
		this.lateSalary = lateSalary;
	}



	public double getAbsenceSalary() {
		return absenceSalary;
	}



	public void setAbsenceSalary(double absenceSalary) {
		this.absenceSalary = absenceSalary;
	}



	public double getWuxianyijin() {
		return wuxianyijin;
	}



	public void setWuxianyijin(double wuxianyijin) {
		this.wuxianyijin = wuxianyijin;
	}



	public double getOvertimeSalary() {
		return overtimeSalary;
	}



	public void setOvertimeSalary(double overtimeSalary) {
		this.overtimeSalary = overtimeSalary;
	}



	public double getBonusSalary() {
		return bonusSalary;
	}



	public void setBonusSalary(double bonusSalary) {
		this.bonusSalary = bonusSalary;
	}



	public String getSalaryTime() {
		return salaryTime;
	}



	public void setSalaryTime(String salaryTime) {
		this.salaryTime = salaryTime;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "SalaryInfoDto [id=" + id + ", employeesName=" + employeesName + ", emId=" + emId + ", sex=" + sex
				+ ", deparemtn=" + deparemtn + ", baseSalary=" + baseSalary + ", lateSalary=" + lateSalary
				+ ", absenceSalary=" + absenceSalary + ", wuxianyijin=" + wuxianyijin + ", overtimeSalary="
				+ overtimeSalary + ", bonusSalary=" + bonusSalary + ", salaryTime=" + salaryTime + "]";
	}



	public SalaryInfoDto(long id, String employeesName, long emId, boolean sex, String deparemtn, double baseSalary,
			double lateSalary, double absenceSalary, double wuxianyijin, double overtimeSalary, double bonusSalary,
			String salaryTime) {
		super();
		this.id = id;
		this.employeesName = employeesName;
		this.emId = emId;
		this.sex = sex;
		this.deparemtn = deparemtn;
		this.baseSalary = baseSalary;
		this.lateSalary = lateSalary;
		this.absenceSalary = absenceSalary;
		this.wuxianyijin = wuxianyijin;
		this.overtimeSalary = overtimeSalary;
		this.bonusSalary = bonusSalary;
		this.salaryTime = salaryTime;
	}



	public SalaryInfoDto(long id, String employeesName, long emId, String deparemtn, double baseSalary,
			double lateSalary, double absenceSalary, double wuxianyijin, double overtimeSalary, double bonusSalary,
			String salaryTime) {
		super();
		this.id = id;
		this.employeesName = employeesName;
		this.emId = emId;
		this.deparemtn = deparemtn;
		this.baseSalary = baseSalary;
		this.lateSalary = lateSalary;
		this.absenceSalary = absenceSalary;
		this.wuxianyijin = wuxianyijin;
		this.overtimeSalary = overtimeSalary;
		this.bonusSalary = bonusSalary;
		this.salaryTime = salaryTime;
	}

	
	
	

}
