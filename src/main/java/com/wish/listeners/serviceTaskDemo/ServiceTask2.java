package com.wish.listeners.serviceTaskDemo;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ServiceTask2 implements JavaDelegate{

	public void execute(DelegateExecution execution) throws Exception {
		System.out.println("I'm " + this.getClass().getName());
	}

}
