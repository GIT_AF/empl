package com.wish.listeners.serviceTaskDemo;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class DepartMentManager implements JavaDelegate{

	public void execute(DelegateExecution execution) throws Exception {
		System.out.println("当前" + execution.getVariable("userName") + "请假" + execution.getVariable("days") + "天！！！");
		System.out.println("####----------------部门经理审批  task !!!");
	}

}
