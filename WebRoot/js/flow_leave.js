var moduleCode = '05001';
var leave = {};
function initFun() {
	if (secure.find) {
		//findDepartment();
		findListInfo();
		dialog = BootstrapDialog.loading();
	}
	if(!secure.add) $('button.add-leave-btn').remove();
	if(secure.add ) $('button.add-leave-btn').removeClass('hide');
}

function findListInfo() {
	
	$.post('./mgr/leave/findAllMyLeaveList', {
		page : page,
	}, function(data) {
		var tbody = $('tbody.tbody-info').empty();
		dialog.close();
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i,v){
			$("<tr></tr>")
			.append($("<td></td>").append(v.id))
			.append($("<td></td>").append(v.employeesName))
			.append($("<td></td>").append(v.startTime))
			.append($("<td></td>").append(v.endTime))
			.append($("<td></td>").append(v.leaveReason))
			.append($("<td></td>").append(v.leaveCurrentNode == null ? "请假单已完成" : v.leaveCurrentNode))
			.append($("<td></td>").append(v.flowState ? "执行中" : "已完成"))
			.append($("<td></td>").append(v.leaveCreateTime))
			.append($("<td></td>").append(analysisBtns(v)))
			.appendTo(tbody);
		});
	}, 'json');
	// 获取分页信息
	$.post('./mgr/leave/findMyLeavePage', {
		page : page
	}, function(data) {
		$.analysisPage(data.body);}, 'json');
	
}


function showLeaveAddBox(){
	$('.empty').removeClass('empty');
	$('input.addID').val("");
	$('input.addStartDate').val("");
	$('input.addEndDate').val("");
	$('input.addLeaveReason').val("");
	BootstrapDialog.showModel($('div.add-box'));
}

function analysisBtns(v){
	var btns = "";
	if(v.flowState){
		
		if(v.leaveCurrentNode == "请假申请"){
			btns += secure.add ? "<button type='button' class='btn btn-primary btn-xs' onclick='showUpdateLeaveBox("+v.id+")'><span class='glyphicon glyphicon-pencil'></span>编辑</button>" : "" ;
			btns += secure.add ? "<button type='button' class='btn btn-success btn-xs' onclick='submitLeave("+v.id+")'><span class='glyphicon glyphicon-align-left'></span>提交</button>" : "" ;
			btns += secure.add ? "<button type='button' class='btn btn-danger btn-xs' onclick='deleteLeave("+v.id+")'><span class='glyphicon glyphicon-align-left'></span>删除</button>" : "" ;
		}else{
			btns += secure.modify ? "<button type='button' class='btn btn-info btn-xs' onclick='showVerifyLeaveBox("+v.id+")'><span class='glyphicon glyphicon-link'></span>审批</button>" : "" ;
			btns += secure.del ? "<button type='button' class='btn btn-danger btn-xs' onclick='deleteLeave("+v.id+")'><span class='glyphicon glyphicon-align-left'></span>删除</button>" : "" ;
		}
		
	}
	
	return btns;
}


/*
 * 显示修改请假单编辑窗口
 * 
 */
function showUpdateLeaveBox(id){
	$('.empty').removeClass('empty');
	if(!id) return;
	leave.id = id;
	dialog = BootstrapDialog.loading();
	$.getJSON('mgr/leave/findLeaveById', {id:id}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		$('input.updateLeaveId').val(id);
		$('input.updateID').val("员工Id："+data.body.employeesId);
		$('input.updateStartDate').val(data.body.startTime);
		$('input.updateEndDate').val(data.body.endTime);
		$('textarea.updateleaveReason').val(data.body.leaveReason);
		BootstrapDialog.showModel($('div.updateLeave-box'));
	});
}
/*
 * 修改员工请假单
 */

function updateLeave(){
	leave.updateLeaveId = $.verifyForm($('input.updateLeaveId'), true);
	leave.updateStartDate = $.verifyForm($('input.updateStartDate'), true);
	leave.updateEndDate = $.verifyForm($('input.updateEndDate'), true);
	leave.updateleaveReason = $.verifyForm($('textarea.updateleaveReason'), true);
	
	if (!$.isSubmit) return;
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/leave/updateLeave', {
		updateLeaveId : leave.updateLeaveId,
		updateStartDate : leave.updateStartDate,
		updateEndDate : leave.updateEndDate,
		updateleaveReason : leave.updateleaveReason,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.updateLeave-box'));
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
		findListInfo();
	}, 'json');
}

/*
 * 确认提交员工请假单
 * te5l.com [K]
 */
function submitLeave(id){
	if(!id) return;
	BootstrapDialog.confirm('是否录提交此请假单!', function(result){
		if(!result) return;
		dialog = BootstrapDialog.loading();
		$.getJSON('./mgr/leave/submitLeave', {id : id}, function(data){
			dialog.close();
			if(!$.isSuccess(data)) return;
			BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
			findListInfo();
		});
	});
}


/*
 * 删除正在运行的员工请假单
 * te5l.com [K]
 */
function deleteLeave(id){
	if(!id) return;
	BootstrapDialog.confirm('是否录删除此请假单!', function(result){
		if(!result) return;
		dialog = BootstrapDialog.loading();
		$.getJSON('./mgr/leave/deleteLeave', {id : id}, function(data){
			dialog.close();
			if(!$.isSuccess(data)) return;
			BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
			findListInfo();
		});
	});
}


/*
 * 添加员工薪酬信息
 * zou
 */
function addLeave(){

	
	
	leave.emId = $.verifyForm($('input.addID'), true);
	leave.addStartDate = $.verifyForm($('input.addStartDate'), true);
	leave.addEndDate = $.verifyForm($('input.addEndDate'), true);
	leave.leaveReason = $.verifyForm($('textarea.leaveReason'), true);
	
	if (!$.isSubmit) return;
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/leave/addLeave', {
		emId : leave.emId,
		startDate : leave.addStartDate,
		endDate : leave.addEndDate,
		leaveReason : leave.leaveReason,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.add-box'));
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
		findListInfo();
	}, 'json');
}

function showVerifyLeaveBox(id){
	$('.empty').removeClass('empty');
	if(!id) return;
	
	leave.id = id;
	dialog = BootstrapDialog.loading();
	$.getJSON('mgr/leave/findLeaveById', {id:id}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		$('input.modifyLeaveId').val(id);
		$('input.leaveDays').val("结束时间："+data.body.endTime);
		$('input.leaveReason').val("请假原因："+data.body.leaveReason);
		$('input.checkResult').val();
		$('input.checkReason').val();
		
		
		BootstrapDialog.showModel($('div.verifyLeave-box'));
	});

	
}


function addVerifyLeaveBox(){
	
	leave.id = $.verifyForm($('input.modifyLeaveId'), true);
	leave.checkResult = $("input[name='checkResult']:checked").val();
	leave.checkReason = $.verifyForm($('textarea.checkReason'), true);
	
	if (!$.isSubmit) return;
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/leave/verifyLeave', {
		id : leave.id,
		checkResult : leave.checkResult,
		checkReason : leave.checkReason,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.verifyLeave-box'));
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
		findListInfo();
	}, 'json');
}


/*
$(function() {
	$('.search-select, .dropdown-menu').on('click', function(e) {
		$target = $(e.target);
		var searchBtn = $('button.search-btn');
		searchBtn.text($target.text());
		searchBtn.append("<span class='caret'></span>");
		searchBtn.attr('name', $target.attr('name'));
	});
	$('select.select-department').change(function(e){
		findPosition($(this).val());
	});
});

 * 新窗口打开新建员请假页面
 * te5l.com [K]
 
function newLeave() {
	window.open('./new_flow_leave.html?moduleCode=05001');
}

 * 获取薪水列表信息
 * te5l.com [K]
 
function findListInfo() {
	var serType = $('button.search-btn').attr('name');
	var serVal = $('input.search-val').val();
	var department = $('select.select-department').val();
	var position = $('select.select-position').val();
	var salaryMonth = $('input.select-leave-time').val();
	if(salaryMonth==undefined){
		salaryMonth = "NoSalaryMonth";
	}
	$.post('./mgr/flow/findAllLeaveList', {
		serType : serType,
		serVal : serVal,
		department : department,
		position : position,
		page : page,
		salaryMonth : salaryMonth
	}, function(data) {
		var tbody = $('tbody.tbody-info').empty();
		dialog.close();
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i,v){
			$("<tr></tr>")
			.append($("<td></td>").append(v.employeesName))
			.append($("<td></td>").append(v.emId))
			.append($("<td></td>").append(v.sex ? '女' : '男'))
			.append($("<td></td>").append(v.deparemtn))
			.append($("<td></td>").append(v.baseSalary))
			.append($("<td></td>").append(v.lateSalary))
			.append($("<td></td>").append(v.absenceSalary))
			.append($("<td></td>").append(v.wuxianyijin))
			.append($("<td></td>").append(v.overtimeSalary))
			.append($("<td></td>").append(v.bonusSalary))
			.append($("<td></td>").append(v.baseSalary+v.lateSalary+v.absenceSalary+v.wuxianyijin+v.overtimeSalary+v.bonusSalary))
			.append($("<td></td>").append(v.salaryTime))
			.append($("<td></td>").append(analysisBtns(v)))
			.appendTo(tbody);
		});
	}, 'json');
	// 获取分页信息
	$.post('./mgr/flow/findLeavePage', {
		serType : serType,
		serVal : serVal,
		department : department,
		position : position,
		page : page
	}, function(data) {
		$.analysisPage(data.body);}, 'json');
	
}

 * 获取部门列表
 * te5l.com [K]
 
function findDepartment(){
	$.getJSON('./mgr/findDepartment', function(data){
		var departmentList = $('select.select-department').empty().append("<option value=0>请选择部门</option>");
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i,v){
			$('<option value='+v.id+'></option>').append(v.name).appendTo(departmentList);
		});
	});
}

 * 根据部门ID, 获取职位列表
 * te5l.com [K]
 
function findPosition(deptId){
	var positionList = $('select.select-position').empty().append("<option value=0>请选择职位</option>");
	if(deptId < 1) return;
	$.getJSON('./mgr/findPositionByDeptId', {deptId : deptId}, function(data){
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i, v){
			$('<option value='+v.id+'></option>').append(v.name).appendTo(positionList);
		});
	});
}

 * 解析按钮组
 * te5l.com [K]
 
function analysisBtns(v){
	var btns = "";
	if(v.salaryTime == null || v.salaryTime == ""){
		btns += secure.modify ? "<button type='button' class='btn btn-primary btn-xs' onclick='showSalaryAddBox("+v.emId+")'><span class='glyphicon glyphicon-pencil'></span>录入</button>" : "" ;
	}else{
		btns += secure.find ? "<button type='button' class='btn btn-success btn-xs' onclick='showSalaryModifyBox("+v.id+")'><span class='glyphicon glyphicon-align-left'></span>编辑</button>" : "" ;
	}
	
	btns += secure.modify ? "<button type='button' class='btn btn-info btn-xs' onclick='testexportExcel("+v.id+")'><span class='glyphicon glyphicon-link'></span>导出</button>" : "" ;
	return btns;
}


*//**
 * /*
 * 直接导出到E：/中
 * @param id
 *//*
function exportExcel(id){
	if(!id) return;
	BootstrapDialog.confirm("请确定是否导出工资单?", function(result){
		if(!result) return;
		dialog = BootstrapDialog.isSubmitted();
		$.getJSON('mgr/salary/exportExcelSalaryById', {id:id}, function(data){
			dialog.close();
			if(!$.isSuccess(data)) return;
			BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
			findListInfo();
		});
	});
}

 * 浏览器下载导出通过id查找的员工工资单
 
function testexportExcel(id){
	if(!id) return;
	BootstrapDialog.confirm("请确定是否导出工资单?", function(result){
		if(!result) return;
		var url="mgr/salary/testexportExcelSalaryById?id="+id;
        window.open(url);

	});
}

 * 浏览器下载导出所有员工工资单
 
function exportAllExcel(){
	var salaryMonth = $('input.select-salary-time').val();
	BootstrapDialog.confirm("请确定是否导出当月所有员工工资单?", function(result){ 
		if(!result) return;
		var url="mgr/salary/exportAllExcelSalary?salaryMonth=" + salaryMonth;
        window.open(url);

	});
}




 * 显示编辑员工薪酬窗口
 
function showSalaryModifyBox(id){
	$('.empty').removeClass('empty');
	if(!id) return;
	
	salary.id = id;
	dialog = BootstrapDialog.loading();
	$.getJSON('mgr/salary/findSalaryById', {id:id}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		$('input.modifyBaseSalary').val(data.body.baseSalary);
		$('input.modifyLateSalary').val(data.body.lateSalary);
		$('input.modifyAbsenceSalary').val(data.body.absenceSalary);
		$('input.modifyWuxianyijin').val(data.body.wuxianyijin);
		$('input.modifyOvertimeSalary').val(data.body.overtimeSalary);
		$('input.modifyBonusSalary').val(data.body.bonusSalary);
		$('input.modifyEmId').val(data.body.emId);
		$('input.modifyid').val(data.body.salaryId);
		
		
		BootstrapDialog.showModel($('div.modify-box'));
	});
}


 * 编辑员工薪酬
 * zou
 
var salary = {};
function modifySalary(){
	
	if(!salary.id) return;
	
	var emId = $.verifyForm($('input.modifyEmId'), true);
	if(!emId) return;
	$.isSubmit = true;
	salary.emId = emId;
	salary.baseSalary = $.verifyForm($('input.modifyBaseSalary'), true);
	salary.lateSalary = $.verifyForm($('input.modifyLateSalary'), true);
	salary.absenceSalary = $.verifyForm($('input.modifyAbsenceSalary'), true);
	salary.wuxianyijin = $.verifyForm($('input.modifyWuxianyijin'), true);
	salary.overtimeSalary = $.verifyForm($('input.modifyOvertimeSalary'), true);
	salary.bonusSalary = $.verifyForm($('input.modifyBonusSalary'), true);
	
	var reg= /^-?\d+(.\d{1,2})?$/;
	
	if(!reg.test(salary.baseSalary) || !reg.test(salary.lateSalary) || !reg.test(salary.absenceSalary) || !reg.test(salary.wuxianyijin) || 
			!reg.test(salary.overtimeSalary) || !reg.test(salary.bonusSalary)){
		BootstrapDialog.msg("输入工资必须为数字，最多有两位小数！！", BootstrapDialog.TYPE_DANGER);
		return;
	}
	
	
	
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/salary/modifySalary', {
		id : salary.id,
		emId : emId,
		baseSalary : salary.baseSalary,
		lateSalary : salary.lateSalary,
		absenceSalary : salary.absenceSalary,
		wuxianyijin : salary.wuxianyijin,
		overtimeSalary : salary.overtimeSalary,
		bonusSalary : salary.bonusSalary,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.modify-box'));
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
		findListInfo();
		
	}, 'json');
}



 * 显示添加薪酬窗口
 * zou
 
function showSalaryAddBox(emId){
	$('.empty').removeClass('empty');
	$('input.baseSalary').val("");
	$('input.lateSalary').val("");
	$('input.absenceSalary').val("");
	$('input.wuxianyijin').val("");
	$('input.overtimeSalary').val("");
	$('input.bonusSalary').val("");
	$('input.emId').val(emId);
	BootstrapDialog.showModel($('div.add-box'));
}



 * 添加员工薪酬信息
 * zou
 
var salary = {};
function addSalary(){
	var emId = $.verifyForm($('input.emId'), true);
	if(!emId) return;
	
	
	salary.emId = emId;
	salary.baseSalary = $.verifyForm($('input.baseSalary'), true);
	salary.lateSalary = $.verifyForm($('input.lateSalary'), true);
	salary.absenceSalary = $.verifyForm($('input.absenceSalary'), true);
	salary.wuxianyijin = $.verifyForm($('input.wuxianyijin'), true);
	salary.overtimeSalary = $.verifyForm($('input.overtimeSalary'), true);
	salary.bonusSalary = $.verifyForm($('input.bonusSalary'), true);
	
	var reg= /^-?\d+(.\d{1,2})?$/;
	
	if(!reg.test(salary.baseSalary) || !reg.test(salary.lateSalary) || !reg.test(salary.absenceSalary) || !reg.test(salary.wuxianyijin) || 
			!reg.test(salary.overtimeSalary) || !reg.test(salary.bonusSalary)){
		BootstrapDialog.msg("输入工资必须为数字，最多有两位小数！！", BootstrapDialog.TYPE_DANGER);
		return;
	}
	
	
	if(!$.isSubmit) return;
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/salary/addSalary', {
		emId : emId,
		baseSalary : salary.baseSalary,
		lateSalary : salary.lateSalary,
		absenceSalary : salary.absenceSalary,
		wuxianyijin : salary.wuxianyijin,
		overtimeSalary : salary.overtimeSalary,
		bonusSalary : salary.bonusSalary,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.add-box'));
		findListInfo();
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
	}, 'json');
}

function selectMonth() {
	WdatePicker({
		dateFmt : 'yyyy-MM',
		isShowToday : false,
		isShowClear : false
	});
} */
